# privacyandroid

use android without google, privacy first and keep location and usage data in our own control

2018 is the year of acknowleging how little privacy end users have, one disclosure after another, we slowly understand how companies and software platforms abuse our lack of knowledge and/or trust, a lot of outlandish or crazy ideas are proved to be true, we need a modern version of tin hat to protect ourself, here is a journal and records of the steps we can take to make android and apps to respect our privacy and keep data local and in our own hands.

## Overview
To protect our privacy from apps, xprivacylua is the perfect tool to use, we need:
1.  a phone we can unlock bootloader and root
2.  install a version of android without google play service
3.  root and install xposed framework
4.  install our final bundle of software to help us protect our data
  * xrivacylua
  * afwall+
  * adaway
  * myandroidtools
  * greenify
5.  other software to fulfill our app need
  * open app store to use opensource equivalent of the popular app
  * yalp to access play store 

## Phone

